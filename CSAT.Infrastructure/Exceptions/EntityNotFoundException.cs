﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSAT.Infrastructure.Exceptions
{
    public class EntityNotFoundException : BusinessException
    {
        private const BusinessExceptionCode InternalExceptionCode = BusinessExceptionCode.EntityNotFound;

        public EntityNotFoundException() : base(InternalExceptionCode)
        {
        }

        public EntityNotFoundException(string message) : base(InternalExceptionCode, message)
        {
        }
    }
}
