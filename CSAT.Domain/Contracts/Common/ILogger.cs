﻿using CSAT.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace CSAT.Domain.Contracts.Common
{
    public interface ILogger
    {
        bool HandleException(LoggingBoundaries boundary, Exception ex);

        bool HandleException(string boundary, Exception ex);

        void LogAudit(LoggingBoundaries boundary, string format, params object[] paramList);

        void LogError(LoggingBoundaries boundary, string format, params object[] paramList);

        void LogInformation(LoggingBoundaries boundary, string format, params object[] paramList);

        void LogTrace(LoggingBoundaries boundary, string format, params object[] paramList);

        void LogWarning(LoggingBoundaries boundary, string format, params object[] paramList);
    }
}
