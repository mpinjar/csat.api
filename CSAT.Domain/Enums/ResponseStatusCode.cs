﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSAT.Domain.Enums
{
    public enum ResponseStatusCode
    {
        Ok = 1,
        BusinessException = 2,
        UnknownException = 3
    }
}
