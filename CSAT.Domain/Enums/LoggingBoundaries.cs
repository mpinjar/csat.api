﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSAT.Domain.Enums
{
    public enum LoggingBoundaries
    {
        Database,
        DataLayer,
        DomainLayer,
        Host,
        ServiceBoundary,
        UI,
        UICritical
    }
}
