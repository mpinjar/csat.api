﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSAT.Domain.Entities
{
    public class PageContainerCombined<T> : PageContainer<T>
    {
        public int UnreadCount { get; set; }

        public int FavoriteCount { get; set; }
    }
}
